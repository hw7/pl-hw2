local
  fun transp ([] :: _) = []
    | transp rows = (map hd rows) :: transp (map tl rows)
  fun norm [] _ = 0
    | norm _ [] = 0
    | norm (x :: xs) (y :: ys) = x * y + (norm xs ys)
  fun rowMul mat row = map (norm row) mat
in
  fun mat_mul [] _ = []
    | mat_mul _ [] = []
    | mat_mul mat1 mat2 = if List.length (List.hd mat1) = List.length mat2 then
      map (rowMul (transp mat2)) mat1 else []
end;

fun implode lst = foldr op^ "" (map str lst);

fun enum lst = foldl (fn (x , y) => y @ [(length(y), x)]) [] lst ;

fun slice lst (start, ending, interval) = foldr (fn ((_, left), lst) => left::lst ) [] (List.filter  (fn (i,x) => (i mod interval) = (start mod interval) andalso i >= start andalso i <= ending ) (enum lst));
