exception Undefined;
exception Empty;

fun initEnv () = fn str : string => raise Undefined;

fun define name env value = 
  fn str : string => if str = name then value else env str;

fun emptyNestedEnv () = [initEnv ()];

fun pushEnv env envStack = env :: envStack;

fun popEnv [] = raise Empty
  | popEnv (_ :: xs) = xs;

fun topEnv [] = raise Empty
  | topEnv (x :: _) = x;

fun defineNested _ [] _ = raise Empty
  | defineNested name (x :: xs) value = (define name x value) :: xs;

fun find (name : string) [] = raise Undefined
  | find name (x :: xs) = x name handle Undefined => find name xs;

