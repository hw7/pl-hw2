# שאלה 1

## א.

```
[o|o]---[o|o]---[o|/]
 |       |       |      
DOLIST   |      [o|o]---[o|o]---[o|o]----------[o|/]
         |       |       |       |              |      
         |      FORMAT   T      "item: ~a~%"    I      
         |      
        [o|o]---[o|/]
         |       |     
         I      LIST    
```



## ב. 

סוגי ליטרלים של ליספ לפי [המסמך](http://www.gigamonkeys.com/book/syntax-and-semantics.html) הם:

* string literals
* literal vectors
* quoted lists
* literal arrays
* numeric literals

## ג.

**REPL** - זה ראשי תיבות שעומדים ל:

* **R**ead - לקרוא את הקלט מכותב התוכנית (לקרוא את הקוד שלו)
* **E**valuate - לעשות חישובים בהתאם לקוד שנקרא בשלב ה-Read
* **P**rint - להדפיס תוצאות של שלבים הקודמים
* **L**oop - לחזרות לשלב ה-Read

בדרך כלל תחת **REPL** מתכוונים לסביבת עבודה עם שפה מסויימת, כך הסביבה מאפשרת להזין חתיחות קוד למפרש (interpreter) ולראות תוצאות הביצוע בתור פלט של המפרש, ואחרי ביצוע של קטע קוד שהוזן, סביבה חוזרת למצב ב-promt (מחכה ל-Read הבא).

אפשר להגדיר **REPL** פשוט לליספ בצורה הבא:

```commonlisp
; @env - initial evaluation environment (before executing @eval)
; Note: @eval updates evaluation environment after it's execution
(define (REPL env)
  (print (eval env (read)))
  (REPL env) )
```